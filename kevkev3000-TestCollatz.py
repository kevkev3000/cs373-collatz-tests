#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# Our unit tests

    # ----
    # Unit tests for collatz_read()
    # ----

    def test_read_01(self):
        s = "9999 109809\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 9999)
        self.assertEqual(j, 109809)

    def test_read_02(self):
        s = "29 3\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  29)
        self.assertEqual(j, 3)

    def test_read_03(self):
        s = "554 8123\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  554)
        self.assertEqual(j, 8123)

    # ----
    # Unit tests for collatz_eval()
    # ----

    def test_eval_01(self):
        v = collatz_eval(55, 10)
        self.assertEqual(v, 113)

    def test_eval_02(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_03(self):
        v = collatz_eval(3012, 80102)
        self.assertEqual(v, 351)

    def test_eval_04(self):
        v = collatz_eval(10, 980)
        self.assertEqual(v, 179)

    def test_eval_05(self):
        v = collatz_eval(160617, 392921)
        self.assertEqual(v, 443)

    def test_eval_06(self):
        v = collatz_eval(236150, 688721)
        self.assertEqual(v, 509)

    def test_eval_07(self):
        v = collatz_eval(10, 510)
        self.assertEqual(v, 144)

    def test_eval_08(self):
        v = collatz_eval(1238, 38409)
        self.assertEqual(v, 324)

    def test_eval_09(self):
        v = collatz_eval(8098, 3248)
        self.assertEqual(v, 262)

    # ----
    # Unit tests for collatz_print()
    # ----

    def test_print_01(self):
        w = StringIO()
        collatz_print(w, 30, 10, 112)
        self.assertEqual(w.getvalue(), "30 10 112\n")

    def test_print_02(self):
        w = StringIO()
        collatz_print(w, 300, 400, 144)
        self.assertEqual(w.getvalue(), "300 400 144\n")

    def test_print_03(self):
        w = StringIO()
        collatz_print(w, 90, 200, 125)
        self.assertEqual(w.getvalue(), "90 200 125\n")

    # ----
    # Unit tests for collatz_solve()
    # ----

    def test_solve_01(self):
        r = StringIO("5000 7000\n2323 79128\n1238 186498\n9 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5000 7000 262\n2323 79128 351\n1238 186498 383\n9 999999 525\n")

    def test_solve_02(self):
        r = StringIO("3275 122\n8764 30\n999 998\n6547 1738\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3275 122 217\n8764 30 262\n999 998 50\n6547 1738 262\n")

    def test_solve_03(self):
        r = StringIO("1 1\n9 9\n112 112\n80123 80123\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n9 9 20\n112 112 21\n80123 80123 170\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.........................
----------------------------------------------------------------------
Ran 25 tests in 0.337s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          73      0     22      0   100%
TestCollatz.py     101      0      0      0   100%
------------------------------------------------------------
TOTAL              174      0     22      0   100%
"""
